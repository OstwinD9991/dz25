<?php


namespace App\Http\Controllers;


use App\Link;
use App\Post;
use Faker\Provider\Uuid;
use http\Client\Request;
use Illuminate\Support\Facades\Auth;

class WallController
{
    public function index(){
        $user= Auth::user();

        $posts=$user->posts()->latest()->get();
        return view('wall', ['posts'=>$posts]);
    }

    public function create(){
        return view('creat-post');
    }
    public function store(\Illuminate\Http\Request $request){
        $request->validate([
            'text' => 'required|min:10',
        ]);

        $text=$request->get('text');
        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $text, $match);

        foreach ($match[0] as $sourceLink){
            $link= new Link;
            $link->short_code=uniqid();
            $link->source_link=$sourceLink;

            Auth::user()->links()->save($link);

            $text=str_replace($sourceLink, sprintf('<a href="%s">%s</a>',  env('APP_URL') . '/r/' . $link->short_code, env('APP_URL') . '/r/' . $link->short_code), $text);
        }


        $post = new Post();
        $post->id=\Ramsey\Uuid\Uuid::uuid4()->toString();
        $post->text = $text;

        Auth::user()->posts()->save($post);

        return redirect()->route('wall.index');

    }
}
