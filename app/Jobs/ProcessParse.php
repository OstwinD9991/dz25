<?php

namespace App\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessParse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $ip;
    protected $UserAgent;
    protected $link;

    public function __construct($ip, $UserAgent, $link)
    {
        $this->ip=$ip;
        $this->UserAgent=$UserAgent;
        $this->link=$link;
    }


    public function handle(  \App\AdapterInterface$adapter,  \App\AdapterInterfaceUserAgent $parsUserAgent)
    {
        $parsUserAgent->parse($this->UserAgent);
        $browser =   $parsUserAgent->getBrowser();
        $engine =  $parsUserAgent->getEngine();
        $os =  $parsUserAgent->getOperatingSystem();
        $device =  $parsUserAgent->getDevice();

        $adapter->parse($this->ip);
        $city = $adapter->getCityName();
        $countryCode = $adapter->getCountryCode();

        $statistic = new \App\Statistic();
        $statistic->id = \Ramsey\Uuid\Uuid::uuid4()->toString();
        $statistic->link_id = $this->link->id;
        $statistic->ip = request()->ip();
        $statistic->user_agent = request()->userAgent();
        $statistic->country_code = $countryCode;
        $statistic->city_name = $city;
        $statistic->browser = $browser;
        $statistic->engine = $engine;
        $statistic->os = $os;
        $statistic->device = $device;
        $statistic->save();

    }
}
