<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

App::singleton(\App\AdapterInterface::class, function ($app) {
//    $reader = new \GeoIp2\Database\Reader(resource_path() . '/GeoLite2/GeoLite2-City.mmdb');
//    return new \App\MaxmindAdapter($reader);
    return new \App\IpapiAdapter();
});

App::singleton(\App\AdapterInterfaceUserAgent::class, function ($app) {
//    $userAgentParser  =  new  \UserAgentParser\Provider\WhichBrowser();
//    return new \App\UserAgentParser($userAgentParser);

    return new \App\WhichBrowserAdapter();
});


Route::get('/', function () {

    $urlgithub = 'https://github.com/login/oauth/authorize';

    $parametersgithub = [
        'client_id' => env('OAUTH_CLIENT_ID'),
        'redirect_uri' => env('OAUTH_REDIRECT_URI'),
        'scope' => 'read:user, user:email'
    ];


    if (\Illuminate\Support\Facades\Auth::check()) {
        $user = \Illuminate\Support\Facades\Auth::user()->only('name');
        return view('welcome', ['user' => $user, 'urlgithub' => $urlgithub . '?' . http_build_query($parametersgithub)]);
    } else {
        return view('welcome', ['urlgithub' => $urlgithub . '?' . http_build_query($parametersgithub)]);
    }
})->name('home');


Route::get('/callbackoauth', function (\Illuminate\Http\Request $request) {


    $client = new GuzzleHttp\Client();
    $response = $client->request('POST', 'https://github.com/login/oauth/access_token', [
        'form_params' => [
            'client_id'=>env('OAUTH_CLIENT_ID'),
            'client_secret'=>env('OAUTH_CLIENT_SECRET'),
            'code'=>$request->get('code'),
            'redirect_uri'=>env('OAUTH_REDIRECT_URI')
        ]
    ]);

    $response = $response->getBody()->getContents();
    $result = [];
    parse_str($response, $result);

    $response = $client->request('GET', 'https://api.github.com/user', [
        'headers' => [
            'Authorization' => 'token ' . $result['access_token']
        ]
    ]);
    $userInfo = json_decode($response->getBody()->getContents(), true);


    $response = $client->request('GET', 'https://api.github.com/user/emails', [
        'headers' => [
            'Authorization' => 'token ' . $result['access_token']
        ]
    ]);
    $userEmails = json_decode($response->getBody()->getContents(), true);


    $email = null;

    foreach ($userEmails as $userEmail) {
        if ($userEmail ['primary'] === true) {
            $email = $userEmail ['email'];
            break;
        }
    }

    $user=\App\User::where('email', '=', $email)->get()->first();

    if (!$user){

        $user = new \App\User();
        $user->name = $userInfo ['name'];
        $user->email = $email;
        $user->email_verified_at = now();
        $user->password =\Illuminate\Support\Facades\Hash::make(\Illuminate\Support\Str::random(10));
        $user->remember_token =\Illuminate\Support\Str::random(10);
        $user->save();

    }
\Illuminate\Support\Facades\Auth::login($user, true);

    return redirect()->route('home');


})->name('callbackoauth');




Route::get('/r/{code}', function ($code) {

   $link = \App\Link::where('short_code', $code)->get()->first();
   $links=\Illuminate\Support\Facades\Cache::add ('code', $code, 86400);
    $UserAgent=$_SERVER['HTTP_USER_AGENT'];
    $ip=request()->ip();
   \App\Jobs\ProcessParse::dispatch($ip, $UserAgent, $link)->onQueue('parser');
    return redirect()->route('home');
  // return redirect($link->source_link);
});

Route::prefix('/admin')->middleware('auth')->group(function () {
    Route::prefix('/users')->group(function () {
        Route::get('/', '\App\Http\Controllers\UserController@index')->name('users.index');
        Route::get('/create', '\App\Http\Controllers\UserController@create')->name('users.create');
        Route::post('/', '\App\Http\Controllers\UserController@store')->name('users.store');
        Route::get('/{user}', '\App\Http\Controllers\UserController@show')->name('users.show');
        Route::get('/user}/edit', '\App\Http\Controllers\UserController@edit')->name('users.edit');
        Route::match(['put', 'patch'], '/{user}', '\App\Http\Controllers\UserController@update')->name('users.update');
        Route::delete('/user}', '\App\Http\Controllers\UserController@destroy')->name('users.destroy');
    });
});


Route::get('/sign-up', '\App\Http\Controllers\SignUpController@index')->name('sign-up');
Route::post('/sign-up', '\App\Http\Controllers\SignUpController@handle')->name('handle-sign-up');
Route::get('/logout', '\App\Http\Controllers\SignUpController@logout')->name('logout-sign-up')->middleware('auth');


Route::get('/sign-in', '\App\Http\Controllers\SignInController@index')->name('login');
Route::post('/sign-in', '\App\Http\Controllers\SignInController@handle')->name('handle-sign-in');


Route::middleware('auth')->group(function () {
    Route::get('/wall', '\App\Http\Controllers\WallController@index')->name('wall.index');
    Route::get('/create-post', '\App\Http\Controllers\WallController@create')->name('post.create');
    Route::post('/create-post', '\App\Http\Controllers\WallController@store')->name('post.store');

});



Route::get('/queue/{message}', function ($message){
    $msg = new \App\Message($message);
    \App\Jobs\ProcessMessage::dispatch($msg)->onQueue('second');

});




//
//Route::resources([
//    'photos' => 'PhotoController',
//    'posts' => 'PostController'
//]);
