@extends('admin.layout')

@section('content')

    <p></p>
    <a class="btn btn-outline-success" href="{{route('users.index')}}" role="button">Return</a>
    <p></p>
    @if(session('message'))
        <div class="alert alert-success">
            {{session('message')}}
        </div>
    @endif
    <table class="table table-striped table-dark">
        <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Email Verified At</th>
            <th scope="col">Created At</th>
            <th scope="col">Updated At</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">{{$users->id}}</th>
                <td>{{$users->name}}</td>
                <td>{{$users->email}}</td>
                <td>{{$users->email_verified_at}}</td>
                <td>{{$users->created_at}}</td>
                <td>{{$users->updated_at}}</td>
            </tr>
        </tbody>
    </table>
@endsection
