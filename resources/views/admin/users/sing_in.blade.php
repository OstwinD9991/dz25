@extends('admin.layout')
@section('content')
    <form class="border border-light p-5">

        <p class="h4 mb-4 text-center">Sign in</p>

        <label for="textInput">Text input Label</label>
        <input type="text" id="textInput" class="form-control mb-4" placeholder="Text input">

        <input type="email" id="defaultLoginFormEmail" class="form-control mb-4" placeholder="E-mail">

        <input type="password" id="defaultLoginFormPassword" class="form-control mb-4" placeholder="Password">

        <label for="passwdInput">Password Label</label>
        <input type="password" id="passwdInput" class="form-control mb-4" placeholder="Password input">

        <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

        <div class="text-center">
            <p>Not a member?
                <a href="">Register</a>
            </p>

            <p>or sign in with:</p>
            <a type="button" class="light-blue-text mx-2">
                <i class="fab fa-facebook-f"></i>
            </a>
            <a type="button" class="light-blue-text mx-2">
                <i class="fab fa-twitter"></i>
            </a>
            <a type="button" class="light-blue-text mx-2">
                <i class="fab fa-linkedin-in"></i>
            </a>
            <a type="button" class="light-blue-text mx-2">
                <i class="fab fa-github"></i>
            </a>
        </div>
    </form>
@endsection
