@extends('admin.layout')

@section('content')

    <p></p>
    <a class="btn btn-outline-success" href="{{route('users.create')}}" role="button">Create User</a>
    <p></p>
    @if(session('message'))
        <div class="alert alert-success">
            {{session('message')}}
        </div>
    @endif
    <table class="table table-striped table-dark">
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Created At</th>
            <th scope="col">Updated At</th>
            <th scope="col">Show</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td><a class="btn btn-outline-info" href="{{route('users.show', $user->id)}}" role="button">Show</a></td>
                <td><a class="btn btn-outline-warning" href="{{route('users.edit', $user->id)}}" role="button">Edit</a></td>
                <td>
                    <form method="post" action="{{route('users.destroy', $user->id)}}">
                        @csrf
                        @method ('delete')
                        <input class="btn btn-outline-danger" type="submit" value="DELETE">
                    </form>
                </td>

            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $users->links() }}
@endsection
