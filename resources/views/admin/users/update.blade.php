@extends('admin.layout')
@section('content')
    <form class="border border-light p-5" method="post" action="{{route('users.update', $user->id)}}">
        @csrf
        @method('put')
        <p class="h4 mb-4 text-center">Update User</p>

        <label for="textInput">Name</label>
        @if($errors->has('email'))
            <ui>
                @foreach($errors->get('name') as $error)
                    <div class="alert alert-danger" role="alert">
                        {{$error}}
                    </div>
                @endforeach
            </ui>
        @endif
        <input type="text" name="name" class="form-control mb-4" placeholder="Name input" value="{{old('name', $user->name)}}">
        <label for="textInput">Email</label>
        @if($errors->has('email'))
            <ui>
                @foreach($errors->get('email') as $error)
                    <div class="alert alert-danger" role="alert">
                        {{$error}}
                    </div>
                @endforeach
            </ui>
        @endif
        <input type="email" name="email" class="form-control mb-4" placeholder="E-mail" value="{{old('email', $user->email)}}">
        <label for="textInput">Password</label>
        @if($errors->has('password'))
            <ui>
                @foreach($errors->get('password') as $error)
                    <div class="alert alert-danger" role="alert">
                        {{$error}}
                    </div>
                @endforeach
            </ui>
        @endif
        <input type="password" name="password" class="form-control mb-4" placeholder="Password">
        <label for="passwdInput">Password Confirmation</label>
        @if($errors->has('password_confirmation'))
            <ui>
                @foreach($errors->get('password_confirmation') as $error)
                    <div class="alert alert-danger" role="alert">
                        {{$error}}
                    </div>
                @endforeach
            </ui>
        @endif
        <input type="password" name="password_confirmation" class="form-control mb-4"
               placeholder="Password Confirmation">

        <button class="btn btn-info btn-block my-4" type="submit">Edit</button>

    </form>
@endsection
