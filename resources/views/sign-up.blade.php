<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">


</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <p>            </p>
            <form class="border border-light p-5" method="post" action="{{route('handle-sign-up')}}">
                @csrf
                <p class="h4 mb-4 text-center">Sign up</p>

                <label for="textInput">Name</label>
                @if($errors->has('name'))
                    <ui>
                        @foreach($errors->get('name') as $error)
                            <div class="alert alert-danger" role="alert">
                                {{$error}}
                            </div>
                        @endforeach
                    </ui>
                @endif
                <input type="text" name="name" class="form-control mb-4" placeholder="Name input" value="{{old('name')}}">
                <label for="textInput">Email</label>
                @if($errors->has('email'))
                    <ui>
                        @foreach($errors->get('email') as $error)
                            <div class="alert alert-danger" role="alert">
                                {{$error}}
                            </div>
                        @endforeach
                    </ui>
                @endif
                <input type="email" name="email" class="form-control mb-4" placeholder="E-mail" value="{{old('email')}}">
                <label for="textInput">Password</label>
                @if($errors->has('password'))
                    <ui>
                        @foreach($errors->get('password') as $error)
                            <div class="alert alert-danger" role="alert">
                                {{$error}}
                            </div>
                        @endforeach
                    </ui>
                @endif
                <input type="password" name="password" class="form-control mb-4" placeholder="Password">
                <label for="passwdInput">Password Confirmation</label>
                @if($errors->has('password_confirmation'))
                    <ui>
                        @foreach($errors->get('password_confirmation') as $error)
                            <div class="alert alert-danger" role="alert">
                                {{$error}}
                            </div>
                        @endforeach
                    </ui>
                @endif
                <input type="password" name="password_confirmation" class="form-control mb-4"
                       placeholder="Password Confirmation">

                <button class="btn btn-info btn-block my-4" type="submit">Sign in</button>

            </form>
        </div>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
